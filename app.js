const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const sql = require('mssql');
const connect = require('connect');

var dbConfig = {
    server: 'localhost',
    database: 'KothaKhali',
    user: 'SA',
    password: 'Your password here',
    port: 1433
};

const app = express();
const server = http.createServer(app);

getEmp = () => {
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.query`select * from Country`
    }).then(result => {
        console.dir(result)
    }).catch(err => {
        console.log(err);
    });
};
const allowedHeaders="Origin, X-Requested-With,Content-Type,Accept,countryId";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));
app.use(connect.cookieParser());
app.use(connect.logger('dev'));
app.use(connect.bodyParser());

app.use(connect.json());
app.use(connect.urlencoded());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", allowedHeaders);
    next();
});

require('./routes/routes.js')(app);
//#region get
app.get('/api/get/city', (req, res) => {
    var countryId = req.get('countryId');

    console.log(`city api called? cid:{countryId} = ` +countryId);
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        if(countryId!=null && countryId>0)
        {
            return pool.request().input('countryId',sql.Int,countryId)
            .query`SELECT [City].[ID]
            ,[City].[Name]
            ,[City].[CountryID]
            ,[Country].[Name] as CountryName
            FROM [KothaKhali].[dbo].[City] Join [dbo].[Country] on [dbo].[City].CountryID = [dbo].[Country].ID 
            where [City].[CountryID]=@countryId`
        }
        else{
            return pool.request().input('countryId',sql.Int,countryId)
            .query`SELECT [City].[ID]
            ,[City].[Name]
            ,[City].[CountryID]
            ,[Country].[Name] as CountryName
            FROM [KothaKhali].[dbo].[City] Join [dbo].[Country] on [dbo].[City].CountryID = [dbo].[Country].ID`
        }
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        console.log(err);
        res.status(500).send(err.message);
    });
});

app.get('/api/get/comment', (req, res) => {
    var roomId = req.get('roomId');
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .query('select * from Comment where [RoomID] = @roomId');
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        console.log(err);
        res.status(500).send(err.message);
    });
});

app.get('/api/get/country', (req, res) => {
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.query`select * from Country`;
    }).then(result => {
        res.json(result.recordsets);
    }).catch(err => {
        console.log(err);
        res.status(500).send(err.message);
    });
});

app.get('/api/get/likes', (req, res) => {
    var roomId = req.get('roomId');
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .query('select * from Likes where [RoomID] = @roomId');
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        console.log(err);
        res.status(500).send(err.message);
    });
});

app.get('/api/get/photo', (req, res) => {
    var roomId = req.get('roomId');
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .query('select * from photo where [RoomID] = @roomId');
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        console.log(err);
        res.status(500).send(err.message);
    });
});

app.get('/api/get/room', (req, res) => {
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.query`SELECT [ID],[Title],[CountryID],[CityID],[Address],[PostedBy],
        [Longitude],[Latitude],[Description],[Price],[NumberOfRooms],[WaterAvailable],[Furnishing]
        FROM [KothaKhali].[dbo].[Room]`
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.get('/api/get/users', (req, res) => {
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.query`SELECT * from Users`
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.get('/api/get/login', (req, res) => {
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('userName', sql.Int, req.params.userName)
        .input('password', sql.Int, req.params.password)
            .query('select * from Users where ([Username] = @userName and [Password]= @password)');
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

//#endregion
//#region post
app.post('/api/post/city', (req, res) => {
    var name = req.body.Name;
    var countryId = req.body.CountryId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('name', sql.NVarChar, name)
            .input('countryId', sql.Int, countryId)
            .query('INSERT INTO [dbo].[City]([Name],[CountryID])' +
            'VALUES(@name,@countryId)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/post/comment', (req, res) => {
    var userId = req.body.userId;
    var roomId = req.body.roomId;
    var comment = req.body.comment;
    var insertDate = req.body.insertDate;  // always pass this format '2017/04/19 12:35 PM'
    var deleteDate = req.body.deleteDate;
    var deleted = req.body.deleted;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('userId', sql.Int, userId)
            .input('roomId', sql.Int, roomId)
            .input('comment', sql.NVarChar, comment)
            .input('insertDate', sql.NVarChar, insertDate)
            .input('deleteDate', sql.NVarChar, deleteDate)
            .input('deleted', sql.Bit, deleted)
            .query('INSERT INTO [dbo].[Comment]([UserID],[RoomID],[Comment],[InsertDate],[DeleteDate],[Deleted])' +
            'VALUES(@userId,@roomId,@comment,@insertDate,@deleteDate,@deleted)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/post/country', (req, res) => {
    var name = req.body.Name;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('name', sql.NVarChar, name)
            .query('INSERT INTO [dbo].[Country]([Name])' +
            'VALUES(@name)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/post/likes', (req, res) => {
    var roomId = req.body.roomId;
    var userId = req.body.userId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .input('userId', sql.Int, userId)
            .query('INSERT INTO [dbo].[Likes]([RoomID],[UserID])' +
            'VALUES(@roomId,@userId)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/post/photo', (req, res) => {
    var roomId = req.body.roomId;
    var path = req.body.path;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .input('path', sql.NVarChar, path)
            .query('INSERT INTO [dbo].[Photo]([RoomID],[Path])' +
            'VALUES(@roomId,@path)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/post/room', (req, res) => {
    var title = req.body.title;
    var countryId = req.body.countryId;
    var cityId = req.body.cityId;
    var address = req.body.address;
    var postedBy = req.body.postedBy;
    var longitude = req.body.longitude;
    var latitude = req.body.latitude;
    var description = req.body.description;
    var price = req.body.price;
    var numberOfRooms = req.body.numberOfRooms;
    var waterAvailable = req.body.waterAvailable;
    var furnishing = req.body.furnishing;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('title', sql.NVarChar, title)
            .input('countryId', sql.Int, countryId)
            .input('cityId', sql.Int, cityId)
            .input('address', sql.NVarChar, address)
            .input('postedBy', sql.Int, postedBy)
            .input('longitude', sql.Real, longitude)
            .input('latitude', sql.Real, latitude)
            .input('description', sql.NVarChar, description)
            .input('price', sql.Real, price)
            .input('numberOfRooms', sql.Int, numberOfRooms)
            .input('waterAvailable', sql.NVarChar, waterAvailable)
            .input('furnishing', sql.NVarChar, furnishing)
            .query('INSERT INTO [dbo].[Room] ([Title],[CountryID],[CityID],[Address]\
            ,[PostedBy],[Longitude],[Latitude],[Description],[Price],[NumberOfRooms],[WaterAvailable]\
            ,[Furnishing])VALUES (@title,@countryId,@cityId,@address,@postedBy,@longitude\
            ,@latitude,@description,@price,@numberOfRooms,@waterAvailable,@furnishing)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/post/users', (req, res) => {
    var userName = req.body.UserName;
    var name = req.body.Name;
    var email = req.body.Email;
    var password = req.body.Password;
    var contactNumber = req.body.ContactNumber;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('userName', sql.NVarChar, userName)
            .input('name', sql.NVarChar, name)
            .input('email', sql.NVarChar, email)
            .input('password', sql.NVarChar, password)
            .input('contactNumber', sql.NVarChar, contactNumber)
            .query('INSERT INTO [dbo].[Users]([Name],[Email],[Username],[Password],[Contactnumber])' +
            'VALUES(@name,@email,@userName,@password,@contactNumber)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});
//#endregion
//#region updates 

app.post('/api/update/city',(req,res)=>{
    var cityId = req.body.cityId;
    var name = req.body.name;
    var countryId = req.body.countryId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('name', sql.NVarChar, name)
            .input('countryId', sql.Int, countryId)
            .input('cityId', sql.Int, cityId)
            .query('UPDATE [dbo].[City] SET [Name]=@name,[CountryID]=@countryId ' +
            'WHERE [ID]=@cityId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/update/comment',(req,res)=>{
    var commentId = req.body.commentId;
    var userId = req.body.userId;
    var roomId = req.body.roomId;
    var comment = req.body.comment;
    var insertDate = req.body.insertDate;  // always pass this format '2017/04/19 12:35 PM'
    var deleteDate = req.body.deleteDate;
    var deleted = req.body.deleted;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('userId', sql.Int, userId)
            .input('roomId', sql.Int, roomId)
            .input('comment', sql.NVarChar, comment)
            .input('insertDate', sql.NVarChar, insertDate)
            .input('deleteDate', sql.NVarChar, deleteDate)
            .input('deleted', sql.Bit, deleted)
            .input('commentId',sql.Int,commentId)
            .query('UPDATE [dbo].[Comment] SET [UserID]=@userId,[RoomID]=@roomId'+
            ',[Comment]=@comment,[InsertDate]=@insertDate,[DeleteDate]=@deleteDate,[Deleted]=@deleted ' +
            'WHERE [ID]=@commentId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/update/country',(req,res)=>{
    var countryId = req.body.countryId;
    var name = req.body.name;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('name', sql.NVarChar, name)
            .input('countryId', sql.Int, countryId)
            .query('UPDATE [dbo].[Country] SET [Name]=@name ' +
            'WHERE [ID]=@countryId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});


//no update required for likes

app.post('/api/update/photo', (req, res) => {
    var roomId = req.body.roomId;
    var path = req.body.path;
    var photoId = req.body.photoId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .input('path', sql.NVarChar, path)
            .input('photoId', sql.NVarChar, photoId)
            .query('UPDATE [dbo].[Photo] SET [RoomID]=@roomId,[Path]=@path ' +
            'WHERE [ID]=@photoId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/update/room', (req, res) => {
    var ID = req.body.ID;
    var title = req.body.title;
    var countryId = req.body.countryId;
    var cityId = req.body.cityId;
    var address = req.body.address;
    var postedBy = req.body.postedBy;
    var longitude = req.body.longitude;
    var latitude = req.body.latitude;
    var description = req.body.description;
    var price = req.body.price;
    var numberOfRooms = req.body.numberOfRooms;
    var waterAvailable = req.body.waterAvailable;
    var furnishing = req.body.furnishing;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('title', sql.NVarChar, title)
            .input('countryId', sql.Int, countryId)
            .input('cityId', sql.Int, cityId)
            .input('address', sql.NVarChar, address)
            .input('postedBy', sql.Int, postedBy)
            .input('longitude', sql.Real, longitude)
            .input('latitude', sql.Real, latitude)
            .input('description', sql.NVarChar, description)
            .input('price', sql.Real, price)
            .input('numberOfRooms', sql.Int, numberOfRooms)
            .input('waterAvailable', sql.NVarChar, waterAvailable)
            .input('furnishing', sql.NVarChar, furnishing)
            .input('roomId',sql.Int,ID)
            .query('UPDATE [dbo].[Room] SET [Title] =@title,[CountryID]=@countryId,[CityID]=@cityId\
            ,[Address]=@address,[PostedBy]=@postedBy,[Longitude]=@longitude,[Latitude]=@latitude,\
            [Description]=@description,[Price]=@price,[NumberOfRooms]=@numberOfRooms,[WaterAvailable]=@waterAvailable\
            ,[Furnishing] = @furnishing WHERE [ID]=@roomId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/update/users', (req, res) => {
    var id = req.body.Id;
    var userName = req.body.UserName;
    var name = req.body.Name;
    var email = req.body.Email;
    var password = req.body.Password;
    var contactNumber = req.body.ContactNumber;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('userName', sql.NVarChar, userName)
            .input('name', sql.NVarChar, name)
            .input('email', sql.NVarChar, email)
            .input('password', sql.NVarChar, password)
            .input('contactNumber', sql.NVarChar, contactNumber)
            .input('id',sql.Int,id)
            .query('UPDATE [dbo].[Users] SET [Name] = @name,[CountryID]=@countryId,[CityID]=@cityId,\
            [Email]=@email,[Username]=@userName,[Password]=@password,[Contactnumber]=@contactNumber \
            WHERE [ID]=@id');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

//#endregion
//#region delete
app.post('/api/delete/city',(req,res)=>{
    var cityId = req.body.cityId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('cityId', sql.Int, cityId)
            .query('DELETE FROM [dbo].[City] ' +
            'WHERE [ID]=@cityId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/delete/comment',(req,res)=>{
    var commentId = req.body.commentId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('commentId',sql.Int,commentId)
            .query('DELETE FROM [dbo].[Comment] \
            WHERE [ID]=@commentId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/delete/country',(req,res)=>{
    var countryId = req.body.countryId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('countryId', sql.Int, countryId)
            .query('DELETE FROM [dbo].[Country] ' +
            'WHERE [ID]=@countryId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/delete/likes', (req, res) => {
    var roomId = req.body.roomId;
    var userId = req.body.userId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .input('userId', sql.Int, userId)
            .query('DELETE FROM [dbo].[Likes]' +
            'WHERE ([RoomID]=@roomId And [UserID]=@userId)');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/delete/photo', (req, res) => {
    var photoId = req.body.photoId;
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('photoId', sql.Int, photoId)
            .query('DELETE FROM [dbo].[Photo]' +
            'WHERE [ID]=@photoId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/delete/room', (req, res) => {
    var roomId = req.body.roomId;

    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('roomId', sql.Int, roomId)
            .query('DELETE FROM [dbo].[Room] WHERE [ID]=@roomId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.post('/api/delete/users', (req, res) => {
    var userId = req.body.userId;
 
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('userId',sql.Int,userId)
            .query('DELETE FROM [dbo].[Users] WHERE [ID]=@userId');
    }).then(result => {
        res.json(result.rowsAffected);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});
//#endregion
//#region check
app.get('/api/check/users/email',(req,res)=>{
    var email = req.get('email');
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('email', sql.NVarChar, email)
            .query('select Count(*) from [Users] where [Email] = @email');
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

app.get('/api/check/users/username',(req,res)=>{
    var username = req.get('username');
    new sql.ConnectionPool(dbConfig).connect().then(pool => {
        return pool.request().input('username', sql.NVarChar, username)
            .query('select Count(*) from [Users] where [Username] = @username');
    }).then(result => {
        res.json(result.recordsets)
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

//#endregion
server.listen(4000, () => {
    console.log('rest service running on port 4000');
});
