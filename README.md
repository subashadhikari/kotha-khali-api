# README #

#Api for Kotha Khali (room search) App

### What is this repository for? ###

* A nodejs api to help Kotha khali app communicate with the database.  

### How do I get set up? ###

* Download the project, add correct login details in app.js. 
* Dependencies : 
	- Express
	- Body-parser
	- http
	- mssql
	- connect
* Add database configuration in app.js's dbconfig object. 

### Who do I talk to? ###

- For more info contact subash.ad00@gmail.com